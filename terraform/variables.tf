// Set variables
variable "yandex_token" {
    description = "Yandex Cloud security OAuth token"
    default     = "" #generate yours by this https://cloud.yandex.ru/docs/iam/concepts/authorization/oauth-token
    type = string
    sensitive = true
}
variable "yandex_cloud_id" {
    description = "Yandex Cloud ID where resources will be created"
    default     = ""
    type = string
    sensitive = true
}
variable "yandex_folder_id" {
    description = "Yandex Cloud Folder ID where resources will be created"
    default     = ""
    type = string
}
variable "env" {
    description = ""
    default     = ""
    type = string
}
variable "fw-ig-name-prod" {
    description = ""
    default     = "fw-ig-prod"
    type = string
}
variable "fw-ig-name-stage" {
    description = ""
    default     = "fw-ig-stage"
    type = string
}
variable "fw-ig-tg-prod" {
    description = ""
    default     = "target-group"
    type = string
}
variable "fw-ig-tg-stage" {
    description = ""
    default     = "target-group-stage"
    type = string
}
variable "fw-load-balancer-name-prod" {
    description = ""
    default     = "fw-load-balancer-prod"
    type = string
}
variable "fw-load-balancer-name-stage" {
    description = ""
    default     = "fw-load-balancer-stage"
    type = string
}
variable "fw-load-balancer-listener-name-prod" {
    description = ""
    default     = "fw-load-balancer-listener-prod"
    type = string
}
variable "fw-load-balancer-listener-name-stage" {
    description = ""
    default     = "fw-load-balancer-listener-stage"
    type = string
}
variable "fw-load-balancer-listener-ip-prod" {
    description = ""
    default     = "51.250.70.198"
    type = string
}
variable "fw-load-balancer-listener-ip-stage" {
    description = ""
    default     = "158.160.62.162"
    type = string
}
variable "fw-ci-name-prod" {
    description = ""
    default     = "fw-ci-prod"
    type = string
}
variable "fw-ci-name-stage" {
    description = ""
    default     = "fw-ci-stage"
    type = string
}
variable "fw-ci-ip-prod" {
    description = ""
    default     = "51.250.76.210"
    type = string
}
variable "fw-ci-ip-stage" {
    description = ""
    default     = "62.84.127.136"
    type = string
}
variable "fw-monitoring-name-prod" {
    description = ""
    default     = "fw-monitoring-prod"
    type = string
}
variable "fw-monitoring-name-stage" {
    description = ""
    default     = "fw-monitoring-stage"
    type = string
}
variable "fw-monitoring-ip-prod" {
    description = ""
    default     = "51.250.76.78"
    type = string
}
variable "fw-monitoring-ip-stage" {
    description = ""
    default     = "51.250.93.202"
    type = string
}
variable "fw-grafana-name-prod" {
    description = ""
    default     = "fw-grafana-prod"
    type = string
}
variable "fw-grafana-name-stage" {
    description = ""
    default     = "fw-grafana-stage"
    type = string
}
variable "fw-grafana-ip-prod" {
    description = ""
    default     = "158.160.59.59"
    type = string
}
variable "fw-grafana-ip-stage" {
    description = ""
    default     = "62.84.126.164"
    type = string
}
variable "fw-vpc-network-name-prod" {
    description = ""
    default     = "fw-vpc-network-prod"
    type = string
}
variable "fw-vpc-network-name-stage" {
    description = ""
    default     = "fw-vpc-network-stage"
    type = string
}
variable "fw-vpc-subnet-name-prod" {
    description = ""
    default     = "fw-vpc-subnet-prod"
    type = string
}
variable "fw-vpc-subnet-name-stage" {
    description = ""
    default     = "fw-vpc-subnet-stage"
    type = string
}
variable "fw-hosts-path-prod" {
    description = ""
    default     = "../ansible/hosts-prod"
    type = string
}
variable "fw-hosts-path-stage" {
    description = ""
    default     = "../ansible/hosts-stage"
    type = string
}
variable "fw-prometheus-path-prod" {
    description = ""
    default     = "../ansible/files/prod/prometheus.yml"
    type = string
}
variable "fw-prometheus-path-stage" {
    description = ""
    default     = "../ansible/files/stage/prometheus.yml"
    type = string
}
