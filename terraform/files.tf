// Create hosts file
resource "local_file" "inventory" {
  filename = "${var.env == "prod" ? var.fw-hosts-path-prod : var.fw-hosts-path-stage}"
  content = <<EOF
[web_servers]
web_server_1 ansible_host=${yandex_compute_instance_group.fw-ig.instances[0].network_interface[0].nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key_file=~/.ssh/id_ed25519
web_server_2 ansible_host=${yandex_compute_instance_group.fw-ig.instances[1].network_interface[0].nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key_file=~/.ssh/id_ed25519

[ci_servers]
ci_server_1 ansible_host=${yandex_compute_instance.fw-ci.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key_file=~/.ssh/id_ed25519

[monitoring_servers]
monitoring_server_1 ansible_host=${yandex_compute_instance.fw-monitoring.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key_file=~/.ssh/id_ed25519

[grafana_servers]
grafana_server_1 ansible_host=${yandex_compute_instance.fw-grafana.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key_file=~/.ssh/id_ed25519
  EOF
}



// Create prometheus.yml file
resource "local_file" "prometheus" {
  filename = "${var.env == "prod" ? var.fw-prometheus-path-prod : var.fw-prometheus-path-stage}"
  content = <<EOF
# Global config
global:
  scrape_interval:     15s # Set the scrape interval to every 15 seconds. Default is every 1 minute.
  evaluation_interval: 15s # Evaluate rules every 15 seconds. The default is every 1 minute.
  external_labels:
      monitor: 'example'

# Alertmanager configuration
alerting:
  alertmanagers:
  - static_configs:
    - targets: ['${yandex_compute_instance.fw-monitoring.network_interface.0.nat_ip_address}:9093']

# Load rules once and periodically evaluate them according to the global 'evaluation_interval'.
rule_files:
  - rules.yml

# A scrape configuration containing exactly one endpoint to scrape:
scrape_configs:
  - job_name: monitoring-prometheus
    static_configs:
      - targets: ['${yandex_compute_instance.fw-monitoring.network_interface.0.nat_ip_address}:9090']
  - job_name: monitoring-node-exporter
    static_configs:
      - targets: ['${yandex_compute_instance.fw-monitoring.network_interface.0.nat_ip_address}:9100']
  - job_name: web1-node-exporter
    static_configs:
      - targets: ['${yandex_compute_instance_group.fw-ig.instances[0].network_interface[0].nat_ip_address}:9100']
  - job_name: web2-node-exporter
    static_configs:
      - targets: ['${yandex_compute_instance_group.fw-ig.instances[1].network_interface[0].nat_ip_address}:9100']
  - job_name: ci-node-exporter
    static_configs:
      - targets: ['${yandex_compute_instance.fw-ci.network_interface.0.nat_ip_address}:9100']
  - job_name: grafana-node-exporter
    static_configs:
      - targets: ['${yandex_compute_instance.fw-grafana.network_interface.0.nat_ip_address}:9100']
  - job_name: web1-blackbox-exporter
    scrape_interval: 30s
    metrics_path: /probe
    params:
      module: [http_2xx] 
    static_configs:
      - targets:
        - http://${yandex_compute_instance_group.fw-ig.instances[0].network_interface[0].nat_ip_address}:8080
    relabel_configs:
      - source_labels: [__address__]
        target_label: __param_target
      - source_labels: [__param_target]
        target_label: instance
      - target_label: __address__
        replacement: ${yandex_compute_instance_group.fw-ig.instances[0].network_interface[0].nat_ip_address}:9115
  - job_name: web2-blackbox-exporter
    scrape_interval: 30s
    metrics_path: /probe
    params:
      module: [http_2xx] 
    static_configs:
      - targets:
        - http://${yandex_compute_instance_group.fw-ig.instances[1].network_interface[0].nat_ip_address}:8080
    relabel_configs:
      - source_labels: [__address__]
        target_label: __param_target
      - source_labels: [__param_target]
        target_label: instance
      - target_label: __address__
        replacement: ${yandex_compute_instance_group.fw-ig.instances[1].network_interface[0].nat_ip_address}:9115
  EOF
}