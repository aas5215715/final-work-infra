// Configure VPC Nework
resource "yandex_vpc_network" "fw-vpc-network" {
  name = "${var.env == "prod" ? var.fw-vpc-network-name-prod : var.fw-vpc-network-name-stage}"
}
resource "yandex_vpc_subnet" "fw-vpc-subnet" {
  name           = "${var.env == "prod" ? var.fw-vpc-subnet-name-prod : var.fw-vpc-subnet-name-stage}"
  zone           = "ru-central1-a"
  network_id     = "${yandex_vpc_network.fw-vpc-network.id}"
  v4_cidr_blocks = "${var.env == "prod" ? ["10.240.1.0/24"] : ["10.240.2.0/24"]}"
}
