// Create a new instance group
resource "yandex_compute_instance_group" "fw-ig" {
  name                = "${var.env == "prod" ? var.fw-ig-name-prod : var.fw-ig-name-stage}"
  folder_id           = var.yandex_folder_id
  service_account_id  = "ajevfbd23rm70tue0tj7"
  instance_template {
    platform_id = "standard-v2"
    resources {
      core_fraction = "20"
      memory = 1
      cores  = 2
    }
    boot_disk {
      initialize_params {
        image_id = "fd82sqrj4uk9j7vlki3q"
        type = "network-hdd"
        size = 15
      }
    }
    network_interface {
      network_id = "${yandex_vpc_network.fw-vpc-network.id}"
      subnet_ids = ["${yandex_vpc_subnet.fw-vpc-subnet.id}"]
      nat = true
    }
    metadata = {
      ssh-keys = "ubuntu:${file("~/.ssh/id_ed25519.pub")}"
    }
  }

  scale_policy {
    fixed_scale {
      size = 2
    }
  }

  allocation_policy {
    zones = ["ru-central1-a"]
  }

  deploy_policy {
    max_unavailable = 1
    max_expansion   = 0
  }

  load_balancer {
    target_group_name        = "${var.env == "prod" ? var.fw-ig-tg-prod : var.fw-ig-tg-stage}"
    target_group_description = "load balancer target group"
  }

}



// Configure load balancer
resource "yandex_lb_network_load_balancer" "fw-load-balancer" {
  name = "${var.env == "prod" ? var.fw-load-balancer-name-prod : var.fw-load-balancer-name-stage}"

  listener {
    name = "${var.env == "prod" ? var.fw-load-balancer-listener-name-prod : var.fw-load-balancer-listener-name-stage}"
    port = 8080
    external_address_spec {
      address = "${var.env == "prod" ? var.fw-load-balancer-listener-ip-prod : var.fw-load-balancer-listener-ip-stage}"
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = "${yandex_compute_instance_group.fw-ig.load_balancer.0.target_group_id}"
    healthcheck {
      name = "http"
      http_options {
        port = 8080
        path = "/"
      }
    }
  }
}




// Create a CI server instance
resource "yandex_compute_instance" "fw-ci" {
  name = "${var.env == "prod" ? var.fw-ci-name-prod : var.fw-ci-name-stage}"
  folder_id = var.yandex_folder_id
  platform_id = "standard-v2"
  zone = "ru-central1-a"

  scheduling_policy {
    preemptible = "true"
  }

  resources {
    core_fraction = "20"
    cores = 2
    memory = 1
  }

  boot_disk {
    initialize_params {
      image_id = "fd82sqrj4uk9j7vlki3q"
      type = "network-hdd"
      size = 15
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.fw-vpc-subnet.id
    nat_ip_address = "${var.env == "prod" ? var.fw-ci-ip-prod : var.fw-ci-ip-stage}"
    nat = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_ed25519.pub")}"
  }

}



// Create a Monitoring server instance
resource "yandex_compute_instance" "fw-monitoring" {
  name = "${var.env == "prod" ? var.fw-monitoring-name-prod : var.fw-monitoring-name-stage}"
  folder_id = var.yandex_folder_id
  platform_id = "standard-v2"
  zone = "ru-central1-a"

  scheduling_policy {
    preemptible = "true"
  }

  resources {
    core_fraction = "20"
    cores = 2
    memory = 1
  }

  boot_disk {
    initialize_params {
      image_id = "fd82sqrj4uk9j7vlki3q"
      type = "network-hdd"
      size = 15
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.fw-vpc-subnet.id
    nat_ip_address = "${var.env == "prod" ? var.fw-monitoring-ip-prod : var.fw-monitoring-ip-stage}"
    nat = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_ed25519.pub")}"
  }

}



// Create a Grafana server instance
resource "yandex_compute_instance" "fw-grafana" {
  name = "${var.env == "prod" ? var.fw-grafana-name-prod : var.fw-grafana-name-stage}"
  folder_id = var.yandex_folder_id
  platform_id = "standard-v2"
  zone = "ru-central1-a"

  scheduling_policy {
    preemptible = "true"
  }

  resources {
    core_fraction = "20"
    cores = 2
    memory = 1
  }

  boot_disk {
    initialize_params {
      image_id = "fd82sqrj4uk9j7vlki3q"
      type = "network-hdd"
      size = 15
    }
  }

  network_interface {
    subnet_id = "${yandex_vpc_subnet.fw-vpc-subnet.id}"
    nat_ip_address = "${var.env == "prod" ? var.fw-grafana-ip-prod : var.fw-grafana-ip-stage}"
    nat = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_ed25519.pub")}"
  }

}
