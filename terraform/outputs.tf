// Show environment
output "Build_environment" {
  value = var.env
}



// Show servers internal IPs
output "Web_server_1_internal_IP" {
  value = "${yandex_compute_instance_group.fw-ig.instances[0].network_interface[0].ip_address}"
}
output "Web_server_2_internal_IP" {
  value = "${yandex_compute_instance_group.fw-ig.instances[1].network_interface[0].ip_address}"
}
output "CI_server_internal_IP" {
  value = "${yandex_compute_instance.fw-ci.network_interface.0.ip_address}"
}
output "Monitoring_server_internal_IP" {
  value = "${yandex_compute_instance.fw-monitoring.network_interface.0.ip_address}"
}
output "Grafana_server_internal_IP" {
  value = "${yandex_compute_instance.fw-grafana.network_interface.0.ip_address}"
}



// Show servers external IPs
output "Web_server_1_external_IP" {
  value = "${yandex_compute_instance_group.fw-ig.instances[0].network_interface[0].nat_ip_address}"
}
output "Web_server_2_external_IP" {
  value = "${yandex_compute_instance_group.fw-ig.instances[1].network_interface[0].nat_ip_address}"
}
output "CI_server_external_IP" {
  value = "${yandex_compute_instance.fw-ci.network_interface.0.nat_ip_address}"
}
output "Monitoring_server_external_IP" {
  value = "${yandex_compute_instance.fw-monitoring.network_interface.0.nat_ip_address}"
}
output "Grafana_server_external_IP" {
  value = "${yandex_compute_instance.fw-grafana.network_interface.0.nat_ip_address}"
}